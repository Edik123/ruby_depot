# localhost:3000/rails/info/routes
Rails.application.routes.draw do

  #root 'store#index'
  #get 'store/index'

  resources :products
  resources :users

  get 'admin' => 'admin#index'

  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  resources :products do
    get :who_bought, on: :member
  end

  scope '(:locale)' do
    resources :orders
    resources :line_items
    resources :carts
    root 'store#index', as: 'store_index', via: :all
  end

  # get    /products        index   products_path
  # get    /products/1      show    product_path(product)
  # get    /products/new    new     new_product_path
  # post   /products        create  products_path
  # get    /products/1/edit edit    edit_product_path(product)
  # patch  /products/1      update  product_path(product)
  # delete /products/1      destroy product_path(product)

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
