class User < ApplicationRecord
  after_destroy :ensure_an_admin_remains
  validates :name, presence: true, uniqueness: true
  has_secure_password

  class Error < StandardError # необязательно создавать ошибку и передавать в raise
  end

private
  # отменяем транзакцию удаления пользователя из бд, если он последний
  def ensure_an_admin_remains
    raise Error, "Can't delete last user" if User.count.zero?
  end

end
